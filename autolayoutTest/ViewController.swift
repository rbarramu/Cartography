//
//  ViewController.swift
//  autolayoutTest
//
//  Created by Jumpitt on 24-05-17.
//  Copyright © 2017 Jumpitt. All rights reserved.
//


import UIKit
import Cartography


class ViewController: UIViewController {
   
    let stackView = UIStackView()
    let titleLabel = UILabel()
    let backgroundView = UIImageView()
    let scanButton = UIButton()
    let blueColor : UIColor = UIColor(red: 48/255, green: 67/255, blue:179/255, alpha: 1.0)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = blueColor
       
        setup()
      
        style()
        render(UIImage(named: "scanQR"))
       // layoutView()
  
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(backgroundView)
        stackView.addArrangedSubview(scanButton)
        
         stackView.translatesAutoresizingMaskIntoConstraints = false;
        
        view.addSubview(stackView)
        
        //Constraints
        stackView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
      
        
    }
    
    
    
}


// MARK: Setup
private extension ViewController{
    func setup(){
        
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 60
       
        


       
    }
}

// MARK: Layout
extension ViewController{
    func layoutView() {
        
        //autolayout the stack view - pin 30 up 20 left 20 right 30 down
        let viewsDictionary = ["stackView":stackView]
        let stackView_H = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-16-[stackView]-16-|",  //horizontal constraint 20 points from left and right side
            options: NSLayoutFormatOptions(rawValue: 0),
            metrics: nil,
            views: viewsDictionary)
        let stackView_V = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-40-[stackView]-120-|", //vertical constraint 30 points from top and bottom
            options: NSLayoutFormatOptions(rawValue:0),
            metrics: nil,
            views: viewsDictionary)
        view.addConstraints(stackView_H)
        view.addConstraints(stackView_V)

    
        

        constrain(titleLabel, backgroundView, scanButton) { label, image, button in
            guard let superview = label.superview else {
                return
            }
            
           //  distribute(by: 30, vertically: label, image, button)
            
            //label.top == superview.top + 40
           // label.leading == superview.leading + 16
            //label.trailing == superview.trailing - 100
            
            button.width == 191
            button.height == 53
          // button.bottom == superview.bottom - 150
            button.centerX == superview.centerX
            
           // image.top == label.bottom + 10
            //image.bottom == button.top - 10
            image.width   == 138
            image.height  == 148
            image.centerX == image.superview!.centerX
            image.centerY == image.superview!.centerY
            
            
            
            
            }
        
      
        
    }
}


// MARK: Style
private extension ViewController{
    func style(){
        
        titleLabel.text = "Escanea el código para \nsincronizar tus datos"
        titleLabel.textColor = UIColor.white
        titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        titleLabel.numberOfLines = 0
        titleLabel.heightAnchor.constraint(equalToConstant: 56).isActive = true;
        titleLabel.widthAnchor.constraint(equalToConstant: 250).isActive = true;

        scanButton.backgroundColor = UIColor.white
        scanButton.setTitle("Escanear", for: .normal)
        scanButton.setTitleColor(blueColor, for: .normal)
        scanButton.heightAnchor.constraint(equalToConstant: 53).isActive = true;
        scanButton.widthAnchor.constraint(equalToConstant: 191).isActive = true;
        
         }
}

// MARK: Render
private extension ViewController{
    func render(_ image: UIImage?){
        if let image = image {
            backgroundView.contentMode = .scaleAspectFill
            backgroundView.clipsToBounds = true
            backgroundView.image = image
            backgroundView.heightAnchor.constraint(equalToConstant: 148).isActive = true;
            backgroundView.widthAnchor.constraint(equalToConstant: 138).isActive = true;
            
        }
    }
    
  }

